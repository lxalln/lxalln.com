---
author: Alex
date: "2016-03-05T11:31:45Z"
description: ""
draft: true
slug: syncing-gitconfig
title: Syncing .gitconfig using symlinks
---

I currently do development on 3 computers:

 - Desktop PC at work
 - Desktop PC at home
 - Laptop at home

I use git for version control, and host our repos on GitHub for our projects at [Koan](http://koan.is) and [Pill Logger](http://github.com/allendevco/pill-logger). I am a big fan of using git from the command line, combining [Cmder](http://cmder.net/), [Posh-Git](https://github.com/dahlbyk/posh-git) (installed with the aid of [GitHub for Windows](http://desktop.github.com), but I'll go into more details about how I work with git day-to-day in another post soon, for now, I want to talk about how I keep my experience in sync across my 3 computers.

We are going to do this by syncing the `.gitconfig` file.

###### Step 1
Find your `.gitconfig` file (below are the default locations on each OS):

 - Windows: `C:\Users\<username>\.gitconfig`
 - Linux: `/home/<username>/.gitconfig`
 - OS X: `$HOME/.gitconfig`

###### Step 2
Move this file to a location that is synced with the cloud, I use Dropbox, but you could use OneDrive, or Google Drive, or whatever you want. Mine is stored at `C:\Users\Alex\Dropbox\Backup\.gitconfig`

###### Step 3
This is where things start to get interesting, we 

[//]: # (Copy file to a synced location (DropBox/OneDrive), create SymLink back to C:\Users\<user>\)

[//]: # (http://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html)

[//]: # (http://www.howtogeek.com/howto/16226/complete-guide-to-symbolic-links-symlinks-on-windows-or-linux/)