---
author: Alex
categories:
- Chrome
- Firefox
- Google
date: "2010-06-16T23:40:13Z"
description: ""
draft: false
slug: firefox-vs-chrome
tags:
- Chrome
- Firefox
- Google
title: Firefox vs Chrome
---

I have been using Firefox since version 0.9 alpha... it's been a long and mostly enjoyable journey. However I have been unable to resist the ever-growing hype surrounding Google Chrome.

So, a couple of weeks ago, I switched. To try to see if I could cope with Chrome as my everyday browser. I thought I'd write a short piece on my experiences:

Chrome 5 dev channel vs Firefox 3.6.4b.

Note that this is not a 'fair' test, Firefox had many many more installed extensions, this is only my observations so please don't complain if I wasn't fair.
<!--more-->

I'll highlight anything that I found to be different or of note between the browsers, if I don't mention something (like page rendering for example), then that's probably because I don't notice a difference.

<strong>Start up time.</strong>
This was pretty much the main reason that I started looking at another browser, my Firefox set up at work takes so long to start up, we're talking minutes rather than seconds, and that's too long for me. I am fairly certain this is due to the number of extensions installed, but Chrome is very very quick at loading up. <strong>Chrome 1-0</strong>

<strong>Adbock Plus</strong>
I hate ads. Actually, that's a little harsh. I hate annoying in my face ads that are irrelevant to me. Unfortunately this is the vast majority of ads that are found on the internet, so I block them. In Firefox, I use the excellent Adblock Plus extension. Therefore I was quite pleased to find this extension available also in Chrome. However, in Chrome (and this is probably a limitation of Chrome rather than the extension writers) the ads show up first and are then actively hidden from me. Causing my page to flicker as it's loading. This is only slightly less annoying than the actual ads themselves. <strong> Firefox levels to 1-1</strong>

<strong>Pinned Tabs</strong>
I first found out about pinned tabs, through an extension in Firefox, however I didn't realise just how much better the pinned tabs are in Chrome, especially in the dev channel. I use the pinned tabs to have Gmail, Twitter and Google Reader open all the time. The great thing (so great in fact, that it might keep me using Chrome) about the pinned tabs in Chrome, is that when the title of the page changes, a subtle glow shines over the tab, alerting me that something has changed (a new email, tweet or reader article). Also typing a new address into a pinned tab, opens it up in a new tab, so that your pinned tabs aren't disturbed. <strong>This is a big win for Chrome, 2-1</strong>

<strong>Memory Usage</strong>
Ok, so Chrome has a separate process for each tab. Whoop-de-do. Firefox almost never crashes for me, I don't tend to go scooting off around the internet onto dodgy sites, and I tend to avoid flash (more on flash in a sec) if I can. Chrome takes up an exorbitant amount of memory. <strong>Firefox equalises again; 2-2</strong>

<strong>Youtube Videos</strong>
For some reason, youtube videos look terrible in Chrome, then shear and tear all over the place. Not sure if this is a dev channel thing or not, but Firefox doesn't have that problem. <strong>2-3 to Firefox</strong>

<strong>Tab Performance</strong>
Seemingly, when a tab has been open for a long time in Chrome, the rendering is removed? I'm not really sure what happens, all I know is that when I come back to a long un-used tab, it's white, and takes a couple of seconds for Chrome to render the contents of the tab again. It's not really a problem, but it's annoying, and a problem I didn't have with Firefox. <strong>Firefox takes a bigger lead; 2-4</strong>

<strong>More On Tabs</strong>
So I'm scrolled half way down a page in Firefox, and I press F5 (refresh). The page reloads (back at the top) then Firefox handily jumps me back to down where I was previously. Chrome seems to try to do this, but more often than not, it's slightly off from where I was before. Which tbh, is damn annoying! Surely it can't be difficult to put me back where I was. Or not even move me at all?? <strong>Firefox romping away now; 2-5</strong>

<strong>Fancy effects</strong>
This is neither hear nor there, but I felt like giving Chrome another point (I did say this wasn't fair). Opening a new tab in Chrome is nice, they slide. Same when closing them. It's just nice. Chrome generally looks newer than Firefox.<strong>Nicer than Firefox; 3-5</strong>

<strong>Extensions</strong>
Firefox has soooo many more extensions than Chrome. I know Chrome is catching up very quickly, but still. There are some extensions such as <a href="https://addons.mozilla.org/en-US/firefox/addon/4072/">Smart Bookmarks Bar</a> that just haven't been produced yet for Chrome. Others such as the <a href="https://addons.mozilla.org/en-US/firefox/addon/3428/">Define</a> extension, are just not possible to do in Chrome due to Google not allowing the user-interface to be modified.
Chrome's only redeeming feature when it comes to extensions, is being able to install and un-install them without having to restart the browser. But still: <strong>Firefox scores again 3-6</strong>


In summary, it seems that Firefox is trouncing Chrome still (for me anyhow). However, those glowing pinned tabs, and the loading time and the look and feel of Chrome make it hard to go back to Firefox.
Perhaps I'll have a look at fixing those 2 issues in Firefox tomorrow, but as of now. <strong>Chrome stays.</strong>