---
author: Alex
categories:
- Football Manager
- Games
- Linux
- Steam
- Ubuntu
date: "2013-08-15T00:00:00Z"
description: ""
draft: false
slug: linux-the-year-of-the-desktop
tags:
- Football Manager
- Games
- Linux
- Steam
- Ubuntu
title: 'Linux: The year of the desktop?'
---

I have been using Linux on and off since I was at college (10 or so years ago now) with a guy called Adam. We used to debate the merits of Mandrake (which has since become <a href="http://wiht.link/mandrivalinux">Mandriva</a>) and Red Hat. This was back in the day when you had to compile your own graphics drivers and the Linux desktop world was pretty horrible.

Fast forward a few years to when I was at university. Still chasing this dream of being able to solely use Linux as a desktop. No longer Mandriva or Red Hat, but now Ubuntu was the distribution of choice. There was 1 thing holding back Linux, 1 thing holding back Ubuntu from truly taking it's place alongside Windows. Games.

The problem with the games on Linux was a never ending cycle of pain. There was very little market or reason for developers to port or develop their games for Linux. This was mainly due to poor hardware support from ATI (now AMD) and Nvidia. The reason they didn't offer great hardware support, was because there was no market. And thus the problem.

A few months ago Valve, that wonderful company who do so much good in the world of gaming, brought to the world their 3rd great deed. The first, was the initial introduction of Steam, to really push the boundaries of digital distribution. The second, was to bring Steam to Mac. The latest and greatest of these great deeds is for Steam to be available on Linux! Thus creating a market, forcing the graphics card developers to up their game and right a wrong that has been bugging me for years.

Anyway, I digress. For although games were the problem, and the thing keeping me locked into the Windows world, there was 1 game in particular, Football Manager.

I have been an owner of Football Manager games since it was called Championship Manager and I was introduced at a young age to CM2 (released in 1995). I said to myself when at university, that if I could get Football Manager to run in Linux, I would be able to ditch Windows and be a happier chap. Alas, I could not. I spent months trying various solutions/hacks, but to no avail.

Yesterday this all changed. Sure it's 6 years later. But on 14th August 2013, SI Games announced that <a href="http://www.footballmanager.com">Football Manager 2014</a> would be available for Linux, through Steam.

I am an intensely happy fellow because of this news. Although I no longer have the time of old, to devote hours upon hours into seasons of football management. I feel I owe it to past Alex to purchase this game and vote with my wallet that this is a <strong>good thing</strong> and that I <strong>approve</strong>!

&nbsp;