---
author: Alex
date: "0001-01-01T00:00:00Z"
description: ""
draft: true
slug: custom-model-binding-interfaces-aspnet-core-2
title: Custom Model Binding Interfaces ASPNET Core 2
---

[//]: # (See OrderEditModelBinder from GiftUp)

This week I have been doing some work with postal addresses. A business requirement to accept postal addresses from around the world.

The addresses are modelled as follows:
```csharp
interface IPostalAddress
{
    string Name { get; set; }
    string AddressLine1 { get; set; }
    string CountryCode { get; }
}

class GBRPostalAddress : IPostalAddress 
{
    … // implementation of IPostalAddress
    public string CountryCode => "GBR";
    public string PostCode { get; set; }
}

class USAPostalAddress : IPostalAddress
{
    … // implementation of IPostalAddress
    public string State { get; set; }
    public string ZipCode { get; set; }
}
```

All well and good so far. Next, we are going to allow the user to edit these addresses.

######View Components
One of my favourite features of aspnet core is view components. They are an excellent way to organise your view code, and make sure that view models only have the data they need. However, for today's use we making use of a view component to keep our view tidy (also potentially for re-use).

```csharp
class EditPostalAddressViewComponent : ViewComponent
{
    public IViewComponentResult Invoke(IPostalAddress address)
    {
        return View(address.CountryCode, address);
    }
}

// GBR.cshtml
﻿@model GBRPostalAddress
<input type="hidden" asp-for="CountryCode"/>

<input asp-for="Name" />
<input asp-for="Address1" />
<input asp-for="PostCode" />

// USA.cshtml
﻿@model USAPostalAddress
<input type="hidden" asp-for="CountryCode"/>

<input asp-for="Name" />
<input asp-for="Address1" />
<input asp-for="State" />
<input asp-for="ZipCode" />

// EditOrder.cshtml
<vc:edit-postal-address address="@Model.Address"></vc:edit-postal-address>
```

_Note: You can invoke view components as tag helpers. [Microsoft Documentation](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/view-components#invoking-a-view-component-as-a-tag-helper)_

Right, so now the view is being rendered and the user can edit the address to their heart's content. However, we're going to have a problem when trying to received the posted form. How are we going to know what address type we are dealing with? _Hint: The clue is in the title of the blog post_

We will have a edit model that looks something like this:
```csharp
class EditOrderModel
{
    public decimal Amount { get; set; }
    … // other fields of the Order

    public IPostalAddress Address { get; set; }
}
```

If you do this, you won't get any exceptions, but it won't work. Your model will come through to your controller action, but `model.Address` will be null. This is because the MVC default model binder, can't create an instance of `IPostalAddress`. So we have to give it a helping hand.

