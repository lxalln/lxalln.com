---
author: Alex
date: "2009-01-23T11:21:20Z"
description: ""
draft: false
slug: supernovi-is-back
title: Supernovi is BACK!
---

Tim has finally got <a href="http://www.supernovi.com">www.supernovi.com</a> back online, with a new and improved theme. Go check it out :)