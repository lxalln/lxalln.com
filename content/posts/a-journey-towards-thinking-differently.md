---
author: Alex
categories:
- apple
- personal
- laptop
date: "2014-03-22T07:26:00Z"
description: ""
draft: false
slug: a-journey-towards-thinking-differently
tags:
- apple
- personal
- laptop
title: A Journey Towards Thinking Differently
---

For ten years I have been wanting a laptop but there have always been one or more reasons preventing me from getting one. Towards the end of last year, the planets aligned and I finally made my first laptop purchase. An Apple MacBook Pro Retina 13". 

<!--more-->

Up until 18 months ago, I spent quite a bit of my spare time playing computer games. This is why I had a decent(ish) gaming PC tower sat in the corner of my flat. This was a big reason for not getting a laptop, the power / quality combination was only available for an extortionate amount of money. Money I did not have. I could upgrade my PC for a few hundred £s, but it would cost me well north of £1000 for a comparable gaming laptop. 18 months I go, I started doing Android development. I found myself going away and just wanting to do a little bit of development, yet finding myself unable (PC towers are big, heavy and unsightly). I needed a laptop.

What little time I'd spend using laptops (I'd had one in a previous job) had meant I'd experienced the issues with battery life that plagued laptops until midway through 2013. Intel launched their new processor line (Haswell) in 2013, which promised a significant increase in battery life, and increased interest from me in laptops.

I have been a somewhat vocal critic of the way that Apple do business. I don't like the iPhone, I don't like its very nature. I do and always have however, appreciated the build quality and attention to detail that Apple take with their hardware. As I am getting more experienced with life (growing up), I am learning that sometimes my stubbornness is harming nobody but myself. My hunt for a laptop was a prime example of this.

Apple announced their new Haswell powered MacBook Pros, which are clearly the best hardware you can buy in notebook form. I found myself having only one reason remaining for not getting a laptop. My dislike for Apple. After weeks of pondering, self deliberation and conversations with my Dad I convinced myself that disliking Apple as a company was not a good enough reason not to buy what (in my opinion) was the closest thing to a perfect laptop for me.

I am now the proud owner of very trendy MacBook Pro, I even have one of those stickers on the back that the cool kids have!

{{< figure src="/images/macbook.jpg" width="100%" >}}

The hardware is full of little joys, a lot of which you don't notice until you switch to using a different laptop.

- The screen is fantastic, the colours, brightness, resolution, everything is great.

- This keyboard is easily one of the best keyboards I've used. There are a couple of layout issues that I'll go into later, but the feedback you get from typing on these keys is just quality.

- Apple have put a trackpad on here, that is so good, I only very rarely plug in an external mouse. Colour me impressed. After years of using laptops, I didn't think this was possible.

- Charging the MacBook is just a better experience than it is on every other laptop. Firstly, it charges very quickly, but more importantly, the MagSafe connector is in my opinion one of the greatest innovations to come from Apple. It's awesome. I still enjoy plugging my MacBook in many months later.

Ultimately I am more than happy with my choice of laptop. I'm glad I spent the extra money and got a premium device. I am not at all disappointed with the hardware. I will very likely buy Apple MacBooks again in the future and don't hestitate to recommend them to anyone looking for a high quality laptop.

However, my elation with Apple as a hardware company did not continue into the software world, but more on that at a later date. I will just finish by saying that my MacBook Pro Retina 13" is a better laptop now it's running Windows 8.1.
