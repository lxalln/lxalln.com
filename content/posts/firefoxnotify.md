---
author: Alex
categories:
- Firefox
- Firefox Add-on
- Linux
- Ubuntu
date: "2009-05-23T18:22:37Z"
description: ""
draft: false
slug: firefoxnotify
tags:
- Firefox
- Firefox Add-on
- Linux
- Ubuntu
title: FirefoxNotify
---

I stumbled on this add-on to Firefox today, finally solving an annoyance I've had for ages. Ubuntu Jaunty has a great new notification system (not perfect, but certainly a step in the right direction), yet Firefox continued to use its own silly notification in the lower left corner of the screen. 

With this add-on the Firefox will utilise the notification system that comes with Ubuntu Jaunty:

{{< figure src="/images/firefoxnotify.png" width="100%" title="Firefox Download Notification" >}}

Get the add-on <a href="https://addons.mozilla.org/en-US/firefox/addon/9622">here</a>.


<strong>Update:</strong> So it seems this only works with the completed download notification. Hopefully the author will integrate it further into Firefox in the future.