---
author: Alex
categories:
- Netflix
- Amazon
- Rants
- TV
- Television
date: "2012-12-10T23:21:10Z"
description: ""
draft: false
slug: state-of-tv
tags:
- Netflix
- Amazon
- Rants
- TV
- Television
title: Want to stream TV? Good luck!
---

{{< figure src="/images/lifestyle_1600_mock.jpg" width="100%" >}}

I have been mulling over writing a post on this topic for most of this year, only now whilst I'm sat in my car waiting for a parking space to become available do I seem angry enough to make a start.

Like most of the western world, I watch a lot of television. Only the way I do this is perhaps not exactly typical. I download TV shows using torrents, serve them on a Linux server in a closet and then stream them to my TVs using Raspberry PIs. This isn't really a post about my setup, that's for another post I have planned. This is a post to explain why I do things the way I do and to share my view of an entirely broken industry.

I download shows because it is currently by far the best experience available for watching TV shows. I am not downloading because I'm cheap and don't want to pay, I would be more than happy to pay for TV. I currently pay ~£70pm on Sky TV, quality service that I'm more than happy to pay for.

"What about Netflix?!" I hear you cry. A fair point. I was more than excited when it was announced Netflix for UK was coming, I could finally ditch all of this complicated setup for a simple service. I couldn't have been more wrong. At the same time Netflix announced their UK service, Amazon gave the public a first taste of their TV streaming offering (LoveFilm). Both of this services have taglines that are variations of the following.
<blockquote><em>"We have thousands of shows that nobody else has."</em></blockquote>
They seem to think that is a good thing, something that will entice me as a customer. Instead, it completely puts me off. Instead of handing over my hard earned pounds to a company to provide me with an all encompassing service to satisfy my TV watching desires, I now have to purchase at the very least two altogether different offerings from different suppliers.

This is on top of my Sky TV subscription which I already need for some content, especially live shows. So instead of making this simpler, I will have to remember which service has which shows. And switch between the two when I want to watch different shows.

As well as having to pay for both services, to watch them on my TV, I need some hardware to do this, luckily I have an Xbox 360 (which both Amazon and Netflix support), great! Except Microsoft require you to purchase an Xbox Live Gold account to use Netflix and/or Amazon. Now I have to pay four different companies to watch TV. Amazon, BSkyB, Microsoft &amp; Netflix.

If that isn't bad enough, the services provided by both Netflix and Amazon are shocking. There is a pitiful amount of content available, with a large amount of high profile shows just simply not available in the UK on either service.

I selected six shows that I watch regularly, some UK shows, some US shows, let's see how the streaming competitors stack up:

ShowNetflixAmazon (LoveFilm)Notes
<table>
<thead></thead>
<tbody>
<tr>
<td>Fringe</td>
<td>X</td>
<td>X</td>
<td>Amazon US</td>
</tr>
<tr>
<td>Merlin</td>
<td>4/5</td>
<td>X</td>
<td></td>
</tr>
<tr>
<td>Suits</td>
<td>X</td>
<td>X</td>
<td>Amazon US</td>
</tr>
<tr>
<td>Game of Thrones</td>
<td>X</td>
<td>X</td>
<td>Amazon Season 1 US</td>
</tr>
<tr>
<td>The Big Bang Theory</td>
<td>X</td>
<td>X</td>
<td>Amazon US</td>
</tr>
<tr>
<td>Sherlock</td>
<td>✓</td>
<td>X</td>
<td>Amazon US</td>
</tr>
</tbody>
</table>
I don't download TV shows because I refuse to pay, or because I want to give the rich media companies the metaphorical finger; I do it because there is no alternative (short of simply not watching TV, which to be honest I have seriously considered many times)

I won't be held to ransom by content providers who are trying to squeeze every last penny out of me.