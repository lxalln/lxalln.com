---
title: "Implement Post Save Hooks EF Core"
date: 2021-02-14T17:12:47Z
draft: true
tags:
  - csharp
  - efcore
  - netcore
---

Sometimes there are actions you want to take, but only once `SaveChanges()` has been called.
This is especially important when working with DI and using a shared `DbContext` instance. You might have commands that should only execute their work if the database gets commited to.

You only want to be calling `SaveChanges()` once the unit of work is complete, this will often be at the end of a controller action. This means you can't call `SaveChanges()` inside your command, but you also don't know if there is more work to do at a higher level, work that may mean that `SaveChanges()` is never called.

For this reason, it's useful to be able to instruct the `DbContext` to execute an action, only once `SaveChanges()` has been called.

We commonly use this for posting to service bus topics, it might look something like this:

```csharp
    public void PostToQueue(Order order)
    {
        // some additional pre work

        _dbContext.AddPostSaveAction(() => _mediator.Send(PostToOrderCreatedQueue.ForOrder(order.Id)));
    }
```

We can then safely return out of this method/command/class and know that our action will be called so long as the database is eventually committed.

```csharp
    public CreateOrder(CreateOrderRequest request)
    {
        var order = new Order();

        // set properties from request model
        
        PostToQueue(order);

        if (FinalValidations())
        {
            // this will call our action after the changes have been commited to the db
            _dbContext.SaveChanges();
        }
    }
```

Ok. Great. Now, how do we do this? EF (legacy) supports this out of the box, but EF Core provides no such support. So we'll have to do it ourselves.

We're going to make use of a `Queue`, which is a great storage mechanism, that we rarely get to use in day-to-day C#.

```csharp
    public class AppDbContext : DbContext 
    {
        // other DbContext stuff

        private readonly Queue<PostSaveAction> _postSaveActions = new Queue<PostSaveAction>();

        public void AddPostSaveAction(Action action)
        {
            _postSaveActions.Enqueue(new PostSaveAction(action));
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            var stateEntriesWritten = base.SaveChanges(acceptAllChangesOnSuccess);

            ExecutePostSaveActions();

            return stateEntriesWritten;
        }

        private void ExecutePostSaveActions()
        {
            while (_postSaveActions.Count > 0)
            {
                var postSaveAction = _postSaveActions.Dequeue();

                switch (postSaveAction.Action)
                {
                    case Action action:
                        action();
                        break;
                }
            }
        }
    }
```

Nice and clean, and reasonably straight forward and understandable.
There are lots of ways to extend this, perhaps there are special cases you'll want to handle for common actions within your code base. We use [Mediatr](https://github.com/jbogard/MediatR) heavily, so we also handle `AddPostSaveAction(INotification notification)`. Sometimes the actions we take require the db to be saved again, so we support that use-case too.
