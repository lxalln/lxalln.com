---
author: Alex
categories:
- committing
- SVN
date: "2010-12-13T11:25:44Z"
description: ""
draft: false
slug: rules-for-committing-to-svn
tags:
- committing
- SVN
title: Rules for committing to SVN
---

I just stumbled upon this sitting on our servers at work, although this is written specifically for us, it should be relevant to anyone using SVN. Hopefully someone will find this useful.

#### Golden Rules

*   [Golden Rule #1](#gr1)
*   [Golden Rule #2](#gr2)

#### FAQ

*   [How often should I commit?](#commitfreq)
*   [Should I work in the trunk, or in a branch?](#trunkorbranch)
*   [What should I do with uncommitted changes?](#uncommittedchanges)

# <a name="gr1">Golden Rule #1</a>

The trunk should ALWAYS build and be stable. Obviously there are going to be times where a bug is introduced into the trunk, but we must do our best to avoid this. Therefore, do not commit changes to the trunk, if those changes cause the project to not build.

# <a name="gr2">Golden Rule #2</a>

At the end of the day, you shouldn’t have uncommitted code on your computer. Hard drives can fail, disasters can happen, your local computer is not backed up. However the servers and repositories are, make sure all code you have been working on, is committed.

### <a name="commitfreq">How often should I commit?</a>

You should commit as often as you think is necessary. However, a rough guideline would be to commit logical ‘blocks’ of code, rather than at time intervals. If you are working on a feature, or fixing a bug, commit the parts that fit together, as soon as they are working. If your bug is to fix a spelling mistake, commit as soon as you have fixed the error.

The reason for committing like this is because it makes it easier to follow the code changes. SVN is not just a place to store code; it is also a history of the changes made. If something you did caused the application to break, it is far easier to identify the problem if you committed in logical ‘blocks’ rather than just at the end of the day, or every hour.

### <a name="trunkorbranch">Should I work in the trunk, or in a branch?</a>

As a general rule, if the change you are making is very small and unlikely to break anything, you can work in the trunk. However, since the trunk needs to be stable all the time ([Golden Rule #1](#gr1)), any more significant changes, or changes that won’t be completed by the end of the day, should be branched.

If in doubt, create a branch and work in there.

### <a name="uncommittedchanges">What should I do with uncommitted changes?</a>

At the end of the day, all code should be commited. But [Golden Rule #1](#gr1) says that unbuildable code shouldn’t be commited. So what happens at the end of the day when your code won’t compile? [Golden Rule #2](#gr2) says it needs to be committed. This is what branches are for. As a general rule if you’re unlikely to finish a change by the end of the day, then you should create a branch and work in there.

At the end of a day you shouldn’t have uncommitted code on your computer.