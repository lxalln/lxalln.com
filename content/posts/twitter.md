---
author: Alex
date: "2009-04-28T14:01:29Z"
description: ""
draft: false
slug: twitter
title: So the badminton season has finished now.
---

Right, so its been about 3 months since I posted here, which to be honest isn't good enough. I've been meaning to come on here and write something, but just never get around to it, so I've been using <a href="http://twitter.com">twitter</a> to share little bits of information now and then.

Since my last post nothing really has happened apart from badminton. I won the Division 1 Evesham Mixed league for Stow which can be seen <a href="http://www.freewebs.com/eveshamleague/tablesmixed1.htm">here</a>. :D
We went into the final match of the season needing a 3-3 draw against the team in 2nd place, who had won the league for the past few years. In predictable style, we lost the first 3 games of the night. We managed to claw our way back, to clinch the title :)

We took the Men's league to the final game of the season, needing a 5-1 victory away at Alcester to be crowned champions and be promoted, in the end, we lost 5-1, ended up finishing 4th in the league, and feeling a bit crap! It was close though, here's how the league finished:
<ul>
	<li>41 | St. Edwards</li>
	<li>40 | Winchcombe B</li>
	<li>39 | Alcester B</li>
	<li><span style="color:#ffff99;">38</span> | <span style="color:#ffff99;">Blackminster B</span></li>
	<li>13 | Winchcombe C</li>
	<li>09 | Bourton B</li>
</ul>