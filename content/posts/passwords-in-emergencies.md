---
author: Alex
date: "2013-01-10T11:05:48Z"
description: ""
draft: false
slug: passwords-in-emergencies
title: Passwords In Emergencies
---

I received a call at 6:20 this morning telling me that one of the websites I work on was down and had been since 01:49. As irritating as this was, it doesn’t happen very often (this is the first time I’ve been woken by a work call) and was down to Windows Update automatically rebooting the server, and the SQL Server service failing to reboot. This is all by-the-by as this post is not really about that.

<!--more-->

Since I hadn’t brought my work laptop home, I didn’t have any of the passwords/account details that I needed to support the application.

I’ll give a little background into the way we handle passwords. For the vast majority of the accounts we need to use, we have randomly generated passwords that are stored in a <a href="http://keepass.info" target="_blank">KeePass</a> file that we share using Dropbox, meaning we can all stay up to date with the passwords. This is a pretty good system, most of the time.

The issue was that on my computer at home, I didn’t have the KeePass file, but that’s ok, it’s in Dropbox. However (you can see where this is going), the password for Dropbox was stored in KeePass. D’oh!

We have now changed the Dropbox account password to something memorable, to hopefully avoid this situation. Lesson learned (the hard way)