---
author: Alex
categories:
- Driving
- Rants
- Speed Cameras
date: "2008-09-06T19:52:52Z"
description: ""
draft: false
slug: speed-cameras
tags:
- Driving
- Rants
- Speed Cameras
title: Speed Cameras
---

Ok, so I have a friend who now live in various places around the country, and over the past month or so I've done a lot of driving. Something you notice very quickly when driving around this 'wonderful' country, is the alarming number of Speed (or should that be Safety) Cameras.

So, there are 3 different types of camera that I've noticed:

Ones in villages.
Ones on country lanes
Ones on motorways/A-roads.

Villages I understand, sometimes to limit people to 30 or 40 miles an hour through a village is totally understandable and doesn't bother me in the slightest as I stick to those speeds anyways.

Country lanes... Seriously? wtf. I don't believe for a second that there is a good 'safety' reason for there to be speed cameras on a country lane. People drive on country lanes as fast as they feel they can (sometimes an issue I know) but they are very unlikely to cause a major incident or risk other people's life.

Motorways! There can be NO reason other than money reasons to put cameras on motorways. These are HIGH-speed roads, they are designed for high speed, accidents on motorways don't come from high speed, they come from low speed, people braking too hard, traffic jams etc.
However, speed cameras are here, and they're here to stay, I can understand why they are there, for money or for safety, whatever! What really really pisses me off, is MORONIC drivers who don't seem to understand how speed cameras work. When you are in a 40MPH limit area, guess what, the speed limit is 40MPH!!! That means you are allowed to do 40MPH! How complicated is that? Exactly, it isn't. So why do countless people insist on panicing when they see a camera..and dropping to 10MPH lower than the limit. The first few times I saw this, I just put it down to the odd person, but not anymore, over the past month it happens almost every time I'm following someone near a camera, they SLOW DOWN!!!

They do it on the country lanes, the villages, the motorways, everywhere! Its so annoying. Come on people, there's a limit, stick to it! No need to drop lower than the limit.