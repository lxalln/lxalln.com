---
author: Alex
categories:
- Headaches
date: "2008-09-28T17:08:18Z"
description: ""
draft: false
slug: headaches
tags:
- Headaches
title: Headaches
---

If you know me, you know that I get headaches, lots of them, if you don't know me, well, I get headaches, lots of them.

Usually, they are mild, and a small dose of paracetamol sorts it out and i can be on my way, doing the things I do during the day.

However, I woke up on saturday morning with a headache, nothing unusual there, so I took a couple of pills, and went about my day then it comes back about 2 hours later and doesn't go away for the rest of the day, it got so bad I couldn't really do anything and just had to go to bed. It was such a waste of a day. I thought that after uni was finished and I moved on with my life and such that I would start getting over these.