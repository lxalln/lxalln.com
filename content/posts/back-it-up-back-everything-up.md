---
author: Alex
categories:
- ghost
- Azure
- backup
date: "2015-04-20T20:46:10Z"
description: ""
draft: false
slug: back-it-up-back-everything-up
tags:
- ghost
- Azure
- backup
title: Back it up. Back everything up.
summary: Ensure you have backups for your content, especially the things you don't touch very often. I lost the content to this blog, but then found it again!
---

This blog is published using [Ghost](http://ghost.org), it has been for a couple of years. When Ghost was released, I was enticed by the open source project, the Shiny New Thing™ as well as the opportunity to get off Wordpress. So with a server already running at home, I decided I could do this self-hosting thing and off I went. In fact I even [wrote about it]({{< relref "hosting-my-own-blog.md" >}}).

Things were fine for a while, until my broadband went down for a week, a couple of days in I realised that this meant my blog was down too! I had a quick poke around at some hosting, but was always too busy to move things.

I moved house about six months ago and ended up with a two month delay before getting broadband setup. During these two months, the SSD in the server began failing, so I reinstalled everything. Well, everything except Ghost. During the past six months, this site has been down, dead, finito. I've thought of a few things I wanted to write during the time, but that meant not just finding the time to write a blog post, but also finding the time to setup Ghost again and make sure the site was working.

Today I was reading an updated [post by Scott Hanselman](http://www.hanselman.com/blog/UPDATEDFor2015HowToInstallTheNodejsGhostBlogSoftwareOnAzureWebAppsAndTheDeployToAzureButton.aspx), about how easy it is now to deploy Ghost to Azure. I use Azure everyday at work, so am definitely comfortable there and decided to bite the bullet and get my blog up and running on Azure, this time actually paying to host (it costs less than £5 a month) and take the burden off my little fileserver at home.

Setting up Ghost was easy, just as Scott describes, the problem came when I thought about importing my data. I SSH'd into the fileserver, only to find no trace of `/var/www/ghost`. I started panicking, but figured that it can't be that bad, because I must have a backup somewhere, right? Wrong! Nothing, I searched through Dropbox, Crashplan, Google Drive, USB pens. It had to be somewhere, I have so many backup solutions, I must have put it somewhere. Apparently not.

Lucky for me, I found the old failing SSD from the fileserver, mounted it and managed to salvage my old data. So the important lesson here, is make sure things are backed up. Go check, make sure all your import things are stored at least in duplicate, if not triplicate. Things break, the longer it's been since they broke, the closer you're getting to the time they do.

My next task is to find a way to backup automatically from Ghost running on Azure.