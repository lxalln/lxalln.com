---
author: Alex
categories:
- "2012"
- New Year
- "2013"
date: "2013-01-04T16:46:19Z"
description: ""
draft: false
slug: it-was-a-very-good-year
tags:
- "2012"
- New Year
- "2013"
title: It was a very good year…
---

I don't really write personal blogs any more, not about my life, I tend to leave all that to <a style="text-decoration: line-through" href="#" target="_blank">Facebook (I don't use Facebook)</a>, <a style="text-decoration: line-through" href="#" target="_blank">Google+ (Google graveyard)</a> or <a href="https://twitter.com/lxalln" target="_blank">Twitter</a>. I want to write more, so here I am with a look back and a glance into the future.

{{< figure src="/images/202724_10151255107277664_214840869_o.jpg" width="100%" >}}

<h4>2012, a brief summary</h4>
I liked 2012, the number looks good. There is something about 2013 that doesn't sit right with me, not sure what it is, maybe it's just that is includes the number 13 quite prominently at the end.

Last year was a pretty huge year for me a lot of things happened, mostly all good! It has ended up with me living in Bristol with my girlfriend Kayleigh and feeling pretty happy with life.

At the start of the year I was feeling pretty down about my job, things weren't going well. Management had decided to basically out-source all development work and turn developers into a support team for all the new third-party software that was being rolled out across the site.

I left. After 4 years of learning my trade and working with some great guys, I moved on. I wasn't the only one, aside from out manager, the entire team (4 devs) walked out within 6 weeks of one another. It was reassuring that the changes weren't just affecting me.

I have ended up in Bristol, which had been on the cards for a while but was a big jump none the less. Not only was I starting a new job and moving to a new city (I'd never really lived in a city before), I was going to be living with someone for the first time. Moving in with Kayleigh was a big step but something we both felt we were ready for. It's been pretty smooth sailing since then!

Not everything has gone to plan this year. My dream of working for a small, edgy startup company came true, it just wasn't all I thought it was cracked up to be. I can't really place the blame at any one person's feet, sometimes things just don't work out. The type of work being done just wasn't what I thought it would be and I've ended up doing a job that really has very little to do with Software Development. Some part of the blame has to be firmly placed on my own shoulders though, I think I was perhaps a little too keen to join a startup and a little desperate to get a job in Bristol. Lessons learned, experienced gained.

The end of year experience was markedly different this year too. Having a split Christmas is something I've never experienced before and something that will take a while to get used to. Things didn't go badly though, we managed to spend plenty of time with both families and even managed to catch up with friends back home. Christmas even brought about an engagement within the group of friends, which means Tim &amp; Laura are almost certainly now going to the first to tie the knot. Congratulations guys!
<h3>The future</h3>
I don't know what 2013 is going to bring, however there are a couple of things on the horizon that I can look forward to.
<ul>
	<li>Jamaica in March - Really looking forward to getting out of the country for the first time since I was 15!</li>
	<li>A new job more in line with the kind of work I'm looking to do. Actual development work. I'm not too fussed about what language that's in, just some programming please.</li>
	<li>I should finally become debt free this year, something I am really excited about.</li>
</ul>
Let's hope 2013 can build on the hard work of 2012.

Happy New Year to all.