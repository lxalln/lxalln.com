---
author: Alex
date: "0001-01-01T00:00:00Z"
description: ""
draft: true
slug: naming-things-is-hard
title: Naming things is hard
---

[//]: # (Start with 'famous' quote about the 2 things that are hard in software development. Naming things and cache invalidation.)


>There are only two hard things in Computer Science: cache invalidation and naming things.

>-- Phil Karlton


One of my favourite sayings. I am not going to talk about cache invalidation, because frankly I'll only get myself in trouble. It's a problem that always seems `n+1` levels deep. I am however, going to put down a few words on something I battle with constantly. Naming things.

Although I'm sure naming all kinds of [things](https://www.theguardian.com/environment/2016/apr/17/boaty-mcboatface-wins-poll-to-name-polar-research-vessel) can be difficult, I want to talk about programming, and more specifically class names.

[//]: # (Give a few examples of obviously bad naming, bad class, bad tests etc.)
Let's start off with an example:


```csharp
public class OrderManagementService
{
    public OrderManagementService(
        IDbConnection dbConnection,
        IMongoConnection mongo,
        IAzureTable<OrderLogEntity> orderLogTable)
    {
        …field assignment…
    }
}
```

We can see above that the example is using Dependency Injection (how you choose to implement DI is a conversation for another day) and currently takes a dependency on 3 outside components.

We'll store the main information about an `Order` in a SQL database using the `IDbConnection`.

In this example our customer data is stored in Mongo, so we need an `IMongoConnection` to access that.

And we're going to keep a log of all things that happen to an `Order` and store that in Azure's Table Storage. That's what the `IAzureTable<OrderLogEntity>` is for.

Let's add some methods to this class.

```csharp
public class OrderManagementService
{
    public OrderManagementService(
        IDbConnection dbConnection,
        IMongoClient mongo,
        IAzureTable<OrderLogEntity> orderLogTable)
    {
        …field assignment…
    }

    public Order Get(Guid orderId)
    {
        // you might want a little more code around here
        // when writing production code
        return _dbConnection.Query<Order>("select * from Order where Id = @Id", new { Id = orderId });
    }

    public Order Save(Order order)
    {
        if(order.Id == Guid.Empty)
        {
            order.Id = Guid.NewGuid();
            _dbConnection.Execute("insert into Order …");
           
           LogOrderActivity(order, "Inserted");       
           return order;
        }

        LogOrderActivity(order, "Updated");
        _dbConnection.Execute("update Order set …");
        return order;
    }

    public IEnumerable<Order> GetForCustomer(Guid customerId)
    {
        return _dbConnection.Query<Order>("select * from Order where CustomerId = @customerId, new { customerId = customerId });
    }

    private void LogOrderActivity(Order order, string activity)
    {
        _orderLogTable.Insert(new OrderLogEntity(order.Id, activity);
    }

    private void UpdateCustomer(Order order)
    {
        var customersCollection = _mongoClient.GetDatabase("ExampleDb").GetCollection("customers");

        // I'd recommend doing a smaller update in production, but this will suffice for now.
        var customer = customersCollection.AsQueryable<Customer>()
                           .Where(c => c.Id == order.CustomerId)
                           .SingleOrDefault();

        // set some properties on the customer
        customer.TotalOrders++;

        customersCollections.UpdateOne(customer);
    }
}
```


[//]: # (Talk about Service as a bad name and the problems it leads to. Maybe link to Mark Seemans post if you can find it.)

[//]: # (Give example of better way to break things up, talk about SRP (Single Responsibility Principle), & testability.)

[//]: # (Sign off)