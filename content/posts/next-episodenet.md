---
author: Alex
date: "2009-01-06T21:28:15Z"
description: ""
draft: false
slug: next-episodenet
title: Next-episode.net
---

Since I'm on a mission to write more posts, I thought I'd write about a site that I visit quite regularly and find very useful.

That site is <a href="http://www.next-episode.net">Next-Episode.net</a>.

A site that is dedicated to helping you track the TV shows that you watch, with a healthy community and active developer in the form of the site's host, santah. The interface is clean, tidy, and very easy to navigate.

Aside from being able to see episode information and schedules, the site has a babe-of-the-day feature maintained by member karenbear.

However, the shining feature of the website is the directly named 'stuff-to-watch', which is a simple and easy way to track the episodes you have watched, as well as those you've yet to see.

Anyway, check it out, its free to use and once you sign up the interface is tidier :)