---
author: Alex
categories:
- IIS
- Development
- URL Rewrite
- Web.config
date: "2012-11-05T18:00:03Z"
description: ""
draft: false
slug: iis-7-url-rewrite-not-installed
tags:
- IIS
- Development
- URL Rewrite
- Web.config
title: IIS 7 URL Rewrite not installed
---

Do you use IIS 7 URL Rewrite? It's a relatively new thing for me to be using and today I stumbled upon an issue that is extremely easy to fix but somewhat difficult to diagnose.

This is the second time I've had this problem so I figured it's time to write something about it, to hopefully help anyone else who comes across this.

This really stems from a 500 - Internal Server error from IIS. When you switch to a view where you can see the error (by logging on to the server locally), you will get this next to useless error from IIS.

{{< figure src="/images/iiserror.png" width="100%" >}}

> "The requested page cannot be accessed because the related configuration data for the page is invalid."

Well that message isn't very useful, but it's ok. These IIS errors show some more information further down.

> Cannot read configuration file
> 
> \?
> 
> D:Websites<Website>web.config

So all IIS is telling us is that it can't read the web.config file. This leads you to think it's one of two possible causes.

*   Permissions
*   Malformed web.config file

However, in this instance, it is neither of these problems. The problem in my case, was that the web.config file has a section that IIS doesn't recognise. URL Rewrite. If you don't have the URL Rewrite module installed, you will most likely get an error that is very hard to debug.

You can install [URL Rewrite](http://www.iis.net/downloads/microsoft/url-rewrite "URL Rewrite") from iis.net using the great Web Platform Installer.

Now I can get back to deploying the website to the new server (that I forgot to install URL Rewrite on).