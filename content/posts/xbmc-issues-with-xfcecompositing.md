---
author: Alex
date: "2011-05-03T19:32:33Z"
description: ""
draft: false
slug: xbmc-issues-with-xfcecompositing
title: XBMC Issues With XFCE/Compositing
---

I have just come across an issue that I struggled to diagnose, I couldn't find this anywhere online, so if only for myself in the future I thought I would write this up here, so hopefully when I google the problem in a year's time, I'll find myself writing about it :)

I have recently switched to using Xubuntu 11.04 for my media PC installation. XBMC is my choice for controlling all of this (I may write another post sometime explaining my setup in more detail, but for now this is about a specific problem).

I use the media box for more than just watching/listening to stuff, not much more, but enough that every now and then I need to come out of XBMC (although without quitting). There is a handy keyboard shortcut to toggle between fullscreen and windowed mode ''.

However, when I switched into windowed mode, all I would see was a black screen and a mouse-cursor (another major bugbear with me and XBMC, but that can wait for another time). After searching online and finding nothing, I decided it was time to just start hacking around until I found something that at least helped me diagnose the problem.

Turns out it didn't take me very long at all, once I'd found the Window Manager settings in XFCE, I noticed that compositing was turned on. Turning this off sorted out my problems straight away. Although I love the compositing features in Linux in general, they are not needed for a media pc, so I have no problems leaving this off.

I hope this helps someone, and if it's me reading this suffering from the same problem again, then you should have remembered this and not wasted so much time looking for the problem again you idiot!