---
author: Alex
categories:
- Coventry
- Ikea
date: "2008-09-28T19:23:38Z"
description: ""
draft: false
slug: a-trip-to-coventry-ikea
tags:
- Coventry
- Ikea
title: A Trip to Coventry Ikea
---

So, I spent most of friday night with a headache (see previous post), when I saw an advert on TV for a laptop. My interested was piqued, so I did some hunting for a laptop, the more I looked, the higher my 'budget' got, before I was looking at spending £700 on a dell laptop. Looked good, but I sat myself down and gave myself a talking to.

I don't need a laptop right now, I wouldn't be happy with a laptop for the kind of money I can afford, so I decided against getting one. However, to further prevent me from wasting my money, I decided to spend some on stuff I could do with.

Cue a trip to Ikea. Off I trot to ikea.com to find the nearest one, which confirmed it was Coventry. Only ever been to coventry once before, 5 years ago, it was crap then and its crap now. Didn't have to intereact with much of Coventry, apart from its awful ring road, first of all, its tiny, there are 'exits' (more on those in a sec) almost every few hundred yard, so its really easy to miss your exit, luckily, it only takes about 5 minutes to do a complete trip around the ring-road, so you can be back to try your luck with the crazy slip-road system.

The slip-roads on the aforementioned Coventry ring-road are joint, as in,  people joining and people leaving have to share the same piece of road, as I have attempted to illustrate below:

{{< figure src="/images/coventry_ring_road.png" width="100%" >}}

Its amazing that they've allowed this to carry on, stupid road. So I think I went around the stupid (its an apt word for this situation) road twice, before I finally found the exit I wanted, but I wasn't done yet, oh no. Although I'd seen the huge super-structure that is IKEA strutting it's stuff in an other wise drab Coventry, actually getting to it, was a whole other matter. Took me another 10 minutes and 2 more trips back onto the ring-road before I made it to a sign that pointed me in the right direction.

Once I finally get to the IKEA car park, I'm pleasantly surprised, as its quite possibly the best car-park I've ever had the 'pleasure' of parking in. They have markers attached to the ceiling, that are either <span style="color:#b00000;">Red</span> or <span style="color:#00b000;">Green</span> depending on whether or not someone is parked in the space. Excellent, although I didn't actually have to use it, as there was a space right near the doors, lucky me.

Now the fun begins... about half way round the 'tour' that IKEA make you go on, I realised I'd left my damn wallet in the car, so I back track to the entrance, where I came in, but OH NO!!!! you can't frickin leave through the entrance, you have to go 6 floors down to the exit floor. I started on the third, went up to the sixth, then back down to the first, sitting on stupid escalators that go so slowly, that morons stand in front of you but REFUSE! to walk on an escalator, like its illegal or something. So once I got out, to my car. got my wallet, I went to the lift (on the first floor) and tried to get back to the third floor (where I wanted to do my shopping), got in the lift, pressed third floor and proceeded to get off on the third floor. BUT FFS!! They don't let you into the actual floor from there, so I went up to the fourth floor, same story, can't get to the actual floor, so I have to go all the way to the sixth floor, be asked the stupid questionairre again and then walk all the way through the sixth, fifth and fouth floors, all so I can get back to the third floor!

All of this, because I left my wallet in the car. My own fault I know, but seriously, it should have been as simple as walking out, getting my wallet and walking back in.

On the plus side, the car park is great, you can take your trolley to your car (unlike Nottingham IKEA) and the people who designed Coventry's road system seem to understand what people want! To get out of Coventry! Leaving was nice and simple.