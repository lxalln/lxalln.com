---
title: "Drinking Coffee"
date: 2019-11-10T12:19:40Z
draft: false
categories:
- Personal
tags:
- Coffee

---

I [promised back in 2013]({{< relref "hosting-my-own-blog.md" >}}) that I would write a follow up post to explore how my experiment with drinking coffee was going. Well, nearly 7 years later, I am back with a conclusion.

<!--more-->

I now drink coffee. Not only that, but I thoroughly enjoy good coffee. I started out drinking black instant coffee with 2 sugars, I now drink 'proper' coffee with no sugars. I dropped the sugar a spoon at a time, and ditched the final spoon when I turned 30. At work we buy in beans roasted in Bristol (where I work), and grind them ourselves. It has taken years to learn to understand how to brew decent coffee, but I get it now.

I don't drink coffee at home during the week, so beans just seem like a waste. I'd constantly be grinding stale beans. Up until recently we had a Bosch Tassimo machine, but to be honest, it wasn't great coffee, miles better than instant, but not great. I took the plunge a couple of months back to switch from Tassimo to Nespresso and haven't looked back.

So to sum it up, I guess my experiment worked. Some say that coffee is an acquired taste, and they are right, certainly in my case.
