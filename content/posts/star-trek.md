---
author: Alex
categories:
- Birmingham
- Star Trek
- Birmingham IMAX
- IMAX
date: "2009-05-23T14:56:16Z"
description: ""
draft: false
slug: star-trek
tags:
- Birmingham
- Star Trek
- Birmingham IMAX
- IMAX
title: Star Trek
---

Last Saturday (16th) I saw Star Trek at the IMAX in birmingham. Overall I thought the film was pretty good, one of the best I've seen this year, but I have some concerns.

This film was one of the rare occasions where I had issue with the quality of the production. Although I'm a bit of a video/audiophile when it comes to quality, I'm usually quite good at getting into a film's story/action. However, the people behind Star Trek seemed obsessed with sun glares all the time, meaning at times (especially the opening scenes), you could barely see what was going on. I've read comments from JJ saying that he admits he was wrong and he was going for a unique style, which I appreciate but I think he went just a bit too far.

Another concerning obsession seemed to be the closeups, made worse by the venue (I'll get to that a bit more later on). Technically its nice to be able to see every single piece of facial hair on Chris Pine's face, but it does detract from the film a little.

So that's the technical side of the film, now onto the story, which at first watch seems very good, impressive, very Star Trek like. I was concerned this wouldn't be Star Trekky enough for me, but it didn't disappoint where that was concerned. Perhaps the bad guy could have been stronger and it was a bit confusing trying to work out just when he was from, but no real issues here. However, when you look back at what was said, this is an 'alternate' time-line. Which is quite clever for them to do, but also very dangerous. This now gives them the freedom to do whatever they like with the Star Trek universe, without concerns for the series that were set further along the 'original' timeline. It might work, but it also runs the risk of alienating the Star Trek fanbase.

Apart from that the film was excellent. The right mixture of action, likeable characters, characters you could dislike without it being annoying and comedy. Simon Pegg could have done with a bigger role but no doubt that will come in future films.

Speaking of future films, I really hope that they don't rush out the next film, as much as I enjoyed this film and can't wait for the next one. Hollywood has a tendency to rush sequels out of the door, in order to cash-in on the hype. This works with some films, where they were already intended to be shot like this (eg. Lord of the Rings) but other films like the Matrix have proved just how badly this can go. I'd rather wait an extra year for a quality film, than go next summer for an average film.

Oh, one more complaint, this time not about the film itself. IMAX is a wonderful technology and I urge you to see the film on an IMAX screen, just not the Birmingham one. The facilities were dire at best. Not even a real popcorn machine, just bags of sweet popcorn. The seats were too close to the screen, no way to book a seat number, so everybody is scrambling to find a seat. Very poor. I might have to wander down to the London one and see if that's any better.

Overall I'll give the film <strong>8/10</strong>, which is high coming from me =)