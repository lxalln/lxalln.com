---
author: Alex
categories:
- Personal
- Project
- Opinion
- Insight
date: "2013-08-07T11:23:16Z"
description: ""
draft: false
slug: working-with-friends
tags:
- Personal
- Project
- Opinion
- Insight
title: Working with Friends
---

I'd like to take a few of your precious moments to talk to you about an experience, or rather collection of experiences, I've had over the past few months with respect to collaborating with friends.

<!--more-->

Not long after Christmas I (and 2 others) set about developing our first app for Android. A Twitter client (yes, yes I know!). This project had a number of goals, that varied amongst the participants.

*   Learn Android
*   Have fun
*   Explore the idea of working together as a team
*   Stay in touch

I had been looking for a personal project to do, that I could 'show off', for a couple of years, nothing had stuck, until this one. I enjoy what I do (programming) but have never really done any projects of my own, outside of work. It was high time I changed that. This project actually stems back to April 2009, when I first registered the domain, procrastination at it's best.

We set up a [Bitbucket](https://bitbucket.org/dashboard) git repository to store our code and a [Trello](http://trello.com) board to keep track of the work that would need doing.

It soon became clear that there was a problem with the dynamic of the team and this is the reason I am writing this.

You see, when you are in the office, working for your company, there is a boss, a purpose (you get paid, right?) and a clear consequence to not doing the work. However, when you are three friends who have set out to do a project in your spare time, there is no appointed boss, there are no clear consequences to not working and this can cause trouble.

When equal stakeholders in a project have differing views on the importance, or urgency of the project, trouble is never far away. People can't be forced to do work, first of all, nobody has the right to tell anyone what to do. Secondly the project was started with the ethos, "do what you want, when you want, if you want". No pressure.

Then, you end up in the situation where one or two of the team aren't taking the project so seriously, or can't put the time in to match the others and this causes tension. They feel like they are being pressured to 'pull their weight', others feel like they are doing all the work and you end up expending a significant amount of energy trying to solve these problems.

The fact that you are already friends outside of the project again makes things a whole lot more complicated. If you started on a project and you got a contractor in, or an anonymous third-party offered to help, it would be easy to cast them off should they not have the same drive/time as the rest of the team. However, when there is a friendship to protect, things get distorted. Punches get pulled, things are forgiven, allowances are made, because a friendship is more important than a project.

I have come to a couple of conclusions over the past few months whilst undertaking this relatively large project with friends.

**Clear Goal**  
Set out a clear goal right from the beginning of the project. Make sure that everyone who is about to be involved, understands what will be involved. Is this a project for fun, or is there more to it than that? If everyone is on the same page from the beginning, the process will be a lot smoother. One of the most crucial mistakes I think I made during this project, was not setting out a clear goal from the outset. This was quite simply because there was no clear goal. I have been guilty throughout the process of letting my goal slip and change. A highly likely event if you don't nail down your goal from the start. Failure to plan is planning to fail.

**Working with Friends**  
Don't start a project with someone just because they are your friend, if you're serious about delivering a result and they're not, it won't work. I have nothing against working with friends on projects, it's a great way to keep in touch and collaborate, but if it's likely to cause trouble, you may end up putting your friendship on the line. Nobody wants to be in that position.

You have to decide whether or not it's worth the risk to your friendship. I know for me that it's something I will have to seriously consider for future projects as I don't like being in a position where I'm having to decide between harming either the project, or the friendship, it's not a nice place to be.

If you find yourself about to embark on a project with friends, or just on your own. I wish you the best of luck.