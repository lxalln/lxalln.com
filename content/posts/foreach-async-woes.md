---
title: "ForEach Async Woes"
date: 2019-11-21T10:00:49Z
draft: false 
summary: Don't use async/await inside List.ForEach in C#, it doesn't work as expected
categories: 
 - Software Engineering
tags:
 - C#
 - async
---

I was investigating a bug last week when I stumbled upon a block of code that looked like this:

```csharp
public async Task Method(List<Animal> collection)
{ 
    // do something async

    collection.ForEach(async v => await _mediator.Send(new Command(v.Id), cancellationToken));
}
```

I wrote this without thinking, I very rarely use List<T>.ForEach() for enumerating through a list, but for some reason I did here. I probably got carried away with the because I can, rather than because I should.

Unfortunately this code is broken, it does not work the way I expected.
Because there is no `await` on the `collection.ForEach...` execution continues on past and the method exits.

Switching to an old school `foreach(...)` loop and awaiting inside in each enumeration will solve this problem.

```csharp
public async Task Method(List<Animal> collection)
{
    // do something async
    
    foreach(var v in collection)
    {
        await _mediator.Send(new Command(v.Id), cancellationToken);
    }
}
```
