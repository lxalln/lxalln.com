---
author: Alex
date: "2007-11-01T21:58:21Z"
description: ""
draft: false
slug: if-only
title: If only...
---

If only, I had someone I could talk to. There is nobody in my life that I can properly talk to about anything and everything, apart from this one person, but now I am really feeling the need to talk about her to someone else, I need to get things off my chest, I need someone to tell me everything  is fine. I need someone who understands what I'm talking about, and someone who won't judge me.

I wish I could talk to the person about this, but I can't, and that sucks. I feel so alone, there's nobody to turn to :(