---
author: Alex
date: "0001-01-01T00:00:00Z"
description: ""
draft: true
slug: what-makes-a-developer-good-to-work-with
title: What makes a developer good to work with
---

[//]: # (Reference stackoverflow survey.)

[//]: # (Passion, Empathy, Humility)


One of the aspects of being a software developer that I am passion about, is the working standard of your day to day. I enjoy removing friction / hurdles from things that you interact with a lot. Sometimes these are big things, sometimes they are small, but removing those friction points is something I like doing.

I spend my time interacting with one of 2 things. Computers, or people. Computers I can control, I can setup my keyboard shortcuts, install my favourite apps, all kinds of things. People on the other hand, I cannot control. However, perhaps I can influence the way they behave, by behaving better myself.

There are 3 areas that I enjoy seeing in other developers, and so try to make sure they they can see in me. Passion, empathy & humility. I believe that if a team of developers are able to individually and collectively project these 3 emotions<!-- emotions??? --> then they will foster a productive and enjoyable working environment.

###### Be passionate
Scott Hanselman has talked about what he calls [dark matter developers](https://www.hanselman.com/blog/DarkMatterDevelopersTheUnseen99.aspx):
> My coworker Damian Edwards and I hypothesize that there is another kind of developer than the ones we meet all the time. We call them Dark Matter Developers. They don't read a lot of blogs, they never write blogs, they don't go to user groups, they don't tweet or facebook, and you don't often see them at large conferences. 

These are the 99% of developers, who churn out the majority of the world's code. However, from my experience, they do not have passion for what they do. They treat development as a job, something that puts food on the table, or enables them to fulfil their true passions in live (whatever they may be).

There is nothing **wrong** about having passions that lie elsewhere, I just don't think they facilitate a productive working environment.

I am passionate, and I find that working with developers who are not, puts a dampener on my own passion. Passion for new tech, passion for producing high quality code, passion for making their work life better.

Conversely, working with other passionate developers is wonderful. Speaking to someone who is passionate about what you are passionate about is what society and community is all about. Getting this at work is extremely fulfilling.

###### Show empathy
[Merriam Webster describes empathy as: ](https://www.merriam-webster.com/dictionary/empathy)
> the action of understanding, being aware of, being sensitive to, and vicariously experiencing the feelings, thoughts, and experience of another…

###### Demonstrate humility