---
author: Alex
categories:
- "2013"
- New Year
- "2014"
date: "2014-01-02T17:07:25Z"
description: ""
draft: false
slug: review-of-2013
tags:
- "2013"
- New Year
- "2014"
title: Review of 2013
---

So, another new year, another blog post reviewing the year gone by. I'll keep it brief!

<!--more-->

## Jamaica
We went to Jamaica this year. Since I've been an adult, this was by far the most exotic holiday I have been on. It was fantastic, it's only a shame we couldn't be away for longer. Next time we will be, although perhaps we'll visit other places on the east coast of America. I wrote a brief post about my time in [Jamaica]({{< relref "jamaica-2013.md" >}}) back in July.

## I got a new Job!
I almost forget that this happened within the last year. Straight after we got back from Jamaica, I started work as the lead developer at [Koan](http://koan.is). It's been a really fun adventure so far and a great learning experience as a developer.
Here are just a few of the things I've been able to work with since I started here:

* MVC
* Azure
* Unit testing
* Selenium
* Raspberry Pi
* Python
* Node.js
* SignalR

I feel that I have progressed a lot as a developer, filling in a lot of the gaps I felt I had in my CV. I'm looking forward to learning a whole lot more in the coming months.
    

## Android development
In the past 12 months, I laboured through writing a Twitter app, only to not release it to the public. Since then I have started a new project, where some of the lessons from previous experiences have been learned, some have not. This is an on-going development experiment, watch this space.

## New Phone
I finally have a Nexus phone and have ditched my long term contract with Orange. At the beginning of November I ordered a Google Nexus 5, which (apart from sending the first one back) I have been delighted with. It gets out of my way, I get to concetrate on the apps I'm using, without having to fight the hardware or the OS.

There is some debate as to whether or not I should have bought the black version instead of the white one. I'm not too fussed with the phone I have. I'm happy.

## Babies, babies, babies!
This past year seems to have been baby central, everywhere I turn there are babies. 

My cousin Ryan and his wife had their first baby (Aria) earlier this year and another cousin (Leonie) has her second.  Our friends Nicci & Simon have had a baby girl (Maisie), other friends Lisa & Simon (I know, I get confused, all the time!) are expecting at the end of January.

I fear this is it now. I don't think I'm going to get away from babies and conversations about babies for the next few years. Once they can talk and walk, I'll be a lot happier I think!

## Onwards.
So, this is 2014. It's a very futuristic sounding date. There is a World Cup this year. That's exciting. Hopefully some big life decisions will be made this year, so by the time I am writing this same post next year, I'll hopefully had an exciting year!

_I think I managed to keep this brief. I'll be back with more posts in the not too distant future._
