---
author: Alex
categories:
- yak shaving
- home automation
- ubuntu
- xbmc
- ghost
date: "2014-01-07T21:54:36Z"
description: ""
draft: false
slug: hosting-my-own-blog
tags:
- yak shaving
- home automation
- ubuntu
- xbmc
- ghost
title: An Exercise In Yak Shaving
---

During the recent Christmas break, I undertook a massive [Yak Shaving](http://en.wiktionary.org/wiki/yak_shaving) session that ended up spanning days.

It started with a new [HDMI Amplifier](http://www.pioneer.eu/eur/products/42/98/405/VSX-323-K/page.html) and has ended with you reading this blog, hosted on my own Ubuntu box using [Ghost](http://ghost.org)!

<!--more-->

I use a Logitech Harmony remote to control my XBMC box, as well as my TV, Amplifier and Xbox 360. With a [Flirc](http://flirc.tv) dongle plugged in to control the XBMC side of things. To cut a reasonably long story short, Flirc have got an official Harmony profile, which required a firmware update to the dongle. This required an update to the software, which didn't work on my installation of 12.04.

I had some spare time (isn't this supposed to be relaxing time of year?) so figured I'd just let Ubuntu do it's upgrade thing. 12.10 downloaded and installed in a couple of hours, rebooted and then nothing, nada, zilch. Oops. One of the many ~~hacks~~ customisations I'd made over the past 2-3 years to this box obviously wasn't playing nicely with the new version of Ubuntu.

This total system failure led me to finally re-installing my media centre on an old 64GB SSD I had lying around, adding an old HDD to act as a backup drive and generally tidying and reorganising. The system boots quickly, and all the services I run now boot themselves up and don't require manual intervention. It was also an excellent (as it turns out) test of my backup solution. I was back up and running in an afternoon.

Filled with confidence from my success, I set about my next challenge. Why not use this always on, file server / media pc to host this blog? Up until now I've used Wordpress.com, which has never really sat well with me, I really don't like the lack of control. I thought about using Wordpress and just hosting that locally, but where's the fun in that?

So I downloaded Ghost, followed the [guide](http://docs.ghost.org/installation/) on the Ghost website and within minutes, had Ghost up and running. All I needed now was my posts, from Wordpress. A quick [Google](http://bit.ly/1ek1r1n) later and I found a plugin that would do exactly what I wanted. The problem? You can't install plugins to your Wordpress.com blog. It would be very useful if Ghost could allow someone to import straight from Wordpress.com, but I understand there are higher priority things for those guys to be working on right now.

What you can do however, is import from Wordpress.com to a hosted Wordpress.org site. So this is what I did. Installed Wordpress locally, imported from Wordpress.com, installed the Ghost plugin, exported and then imported into Ghost!

After a few minor tweaks to the theme and setting up [Disqus](http://www.disqus.com) for comments, I was up and running. You are reading this blog and it's hosted in my living room! 

I like that I own my data, it feels good.