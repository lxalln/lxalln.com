---
author: Alex
categories:
- domain mapping
- google apps
date: "2009-01-06T20:44:06Z"
description: ""
draft: false
slug: domain-changes
tags:
- domain mapping
- google apps
title: Domain changes
---

Howdy there, its been a while, but one of my resolutions of 2009 is to keep this place more updated.

In october last year, I bought a couple of domain names, completenutter2.co.uk and completenutter2.com, today I finally got around to setting them up to work with wordpress using the domain mapping feature, so this should be on completenutter2.co.uk now, which is much nicer than completenutter2.wordpress.com.

I've also set up emails on this domain using <a href="https://www.google.com/a/">google apps</a>, which is yet another gem that google have kept hidden from me!

So you can now email me on the following address: <strong>alex</strong> <em>at</em> <strong>completenutter2</strong> <em>dot</em> <strong>co</strong> <em>dot</em> <strong>uk</strong>

Anyway, I'm done for now, back soon with some more.

Oh, and there's <a href="http://www.twitter.com/lxalln">twitter</a> too.