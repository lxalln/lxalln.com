---
author: Alex
date: "2009-11-09T21:18:34Z"
description: ""
draft: false
slug: badminton-new-season
title: Badminton - New Season
---

So,

I know that I haven't posted an update to this thing since my most read post about google wave (since then I have received an invite, got all excited, then realised its too buggy for everyday use). Anyway, that isn't what this post is about.

I'm going to talk about badminton. The new season has started, and this time round I'm playing in 3 teams. The same 2 from last year, Blackminster B team, should have got into the A team, but my form hasn't been good enough and Stow's mixed team, where we are the defending champions.

I'm also playing in the Worcester league with Blackminster, its a new format, and what seems like a higher standard (we're playing in the 5th division, and we're by no means walking it).

Although we've had some good games so far this year, I am yet to lose an overall match (shouldn't say that, I have 2 matches in the next 2 days). I've enjoyed playing at the higher standard of the Worcester league, because I really struggle for motivation sometimes. I wish I was playing high quality matches all the time, because I feel I could be a better player than I am.

I'll try and post more stuff about things going on. Of course you can keep up with me on twitter.com/completenutter2 :)