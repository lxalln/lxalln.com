---
author: Alex
categories:
- raspbmc
- raspberry pi
date: "2012-10-23T21:49:45Z"
description: ""
draft: false
slug: raspbmc-restoration-woes
tags:
- raspbmc
- raspberry pi
title: Raspbmc Restoration Woes
---

When my second Raspberry Pi turned up, I decided that I could set them up based on the one I already had working and running the excellent raspbmc. To do this, I would install raspbmc, then copy the settings files from the old SD card to the new one.

Since I wanted to run the 2 exactly the same, I didn't see a problem with doing this. All seemed well, the Pi booted, things looked the same as they did on the first Pi, the library was working fine, I could see all of my movies & TV shows listed as they should be.

Trouble arose when I tried to play anything. The Pi froze. Simply crashed, no errors, no log files, no nothing. Since Raspbmc has not yet reached a release version, I figured it was something to do with this. Ultimately, this was unfair, as the issue purely rested at my feet.

I had forgotten to grant write access to the config files I had copied across, so although the Pi could read the settings I copied across, it could not write to them. Which it obviously was try to do when starting a video.

Problem solved. Grant access to the copied files and all works excellently again. I can go back to enjoying the excellent experience provided by Sam and the Raspbmc team.