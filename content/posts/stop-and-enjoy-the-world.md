---
author: Alex
categories:
- World
date: "2013-02-27T19:35:04Z"
description: ""
draft: false
slug: stop-and-enjoy-the-world
tags:
- World
title: Stop and Enjoy the World
---

I was stood in the centre of Bristol a few days ago, waiting to meet Kayleigh. Looking around, watching Bristol pass me by I glanced toward the sky.

It was a beautiful sky and a plane was over head, leaving a sunset-lit trail. My first thought was "I wish I could get a decent picture of this to capture this moment."

It hit me that I was actually spending my time thinking about the type of camera/lens I would need to get a good picture, where I'd share it, what people would think etc. I should have been just taking in the sight and enjoying the moment.

It seems that the world is becoming obsessed with capturing our moments, rather than enjoying them. Take a look at any concert or gig and you'll see something like this:

{{< figure src="/images/wpid-trainfordsony241.jpg" width="50%" >}}

What are these people hoping to achieve? The image will be crap, the sound won't be worth listening to. Their ticket was proof they were there. I can only assume that these people are acting under the assumption that this will help them hold on to the experience. Even though watching a video recorded on your phone in the dark is not going to be even close to the experience they could have been having had they simply put their phones away.

The world is alive, there are new experiences to be had everyday. We need to stop living in the past and forever looking back. Go out and live a little.