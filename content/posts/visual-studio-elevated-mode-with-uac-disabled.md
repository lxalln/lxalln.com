---
author: Alex
categories:
- Development
- UAC
- Visual Studio
- Windows
date: "2014-04-04T16:54:20Z"
description: ""
draft: false
slug: visual-studio-elevated-mode-with-uac-disabled
tags:
- Development
- UAC
- Visual Studio
- Windows
title: Visual Studio in Elevated Mode with UAC disabled
summary: If you need Visual Studio to run in Elevated Mode, but also have UAC turned off. This is how to solve that particular issue.
---

_Note: I lost the images in this post sometime when I switched blog hosting_

I am currently working on a project that is heavily reliant on Windows Azure, which means we use the Azure emulator locally. Although not a perfect emulation, it serves it's purpose for supporting local development.

Running the Azure emulator requires you to launch Visual Studio as an Administrator. The manual way to do this is to right click on the Visual Studio icon in the task bar, then right on the Visual Studio link, then click on `Run as administrator`

![Run visual studio as administrator](/content/images/2015/04/Untitled_Clipping_040414_035134_PM.jpg)

However, this gets old, fast. Especially when you forget to do this, you run Visual Studio, then launch your Azure project to be greeted with the warning dialog telling you that you need to run Visual Studio in elevated mode.

![The Windows Azure compute emulator must be run elevated.](/content/images/2015/04/dq1dwfjo_040414_035607_PM-1.jpg)

There are dozens of posts out there telling you to fix this. Which basically boils down to telling Windows to always run this applications as Administrator. The problem comes when you have UAC disabled. UAC has always driven me up the wall, so one of the first things I do with a new PC is disable UAC. I know the risks of doing this, I accept full responsbility for the death of my PC for doing this, but I can't abide the irritating popups all the time for doing simple tasks.

So, if you, like me disable UAC, you will find that Visual Studio doesn't run as Administrator unless you explicitly click the `Run as administrator` button. Which as we already established, is annoying.

There is a ~~fix~~ workaround! I don't remember where I first read about this, but if anyone knows, I'll update this post to give them credit. If go to the actual executable `devenv.exe` (which on my system is in `C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\`), right click and select `Troubleshoot Compatability` 

You'll be presented with the following screen, which you'll need to select `Troubleshoot program`.

![Program Compatability Troubleshooter](/content/images/2015/04/Screenshot_040414_040728_PM.jpg)

On the next screen, select `The program requires additional permissions`, then make sure you click the `Test the program...` button. Then you're set. Visual Studio will run (even when you use the jump list to select a project) as Administrator.

I always forget how to do this, so this will at the very least, serve as a reminder to me.

**Update:** I found the link I was looking for. Of course, it was on [Stack Overflow](http://stackoverflow.com/a/12859334/206448).

**Update:** This setting will cause an annoying screen to popup when opening a solution file directly from explorer. You can fix this by applying the same additional permissions to `VSLauncher.exe` which can be found at: `C:\Program Files (x86)\Common Files\Microsoft Shared\MSEnv\vslauncher.exe`