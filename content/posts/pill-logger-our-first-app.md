---
author: Alex
categories:
- Pill Logger
- Development
- Android
date: "2014-02-15T23:02:14Z"
description: ""
draft: false
slug: pill-logger-our-first-app
tags:
- Pill Logger
- Development
- Android
title: Pill Logger, our first app.
---

Last week my brother Nick and I, released our first Android app. [Pill Logger](https://play.google.com/store/apps/details?id=uk.co.pilllogger) is a simple application to keep track of when you take medicine. For those people who often forget when they last took a pill, or who want to look over their medicine taking history and view their trends.

<!--more-->

It's a simple application that has a deliberately small feature set. After our first attempt at releasing an app went horribly wrong, we decided that this time it was ~~important~~ vital to at least try to go about things in a different way. The core principle being 'Release Early, Release Often'. With Twitalot (our first app), we spent far too much time and effort adding features and far too little time actually releasing anything. 9 months in, we cancelled development, having released precisely nothing.

With Pill Logger, the main aim is to experience the full development cycle, get something released and learn. Learn from the development, learn from the release, learn from the users.

I am looking forward to the future of Pill Logger and whatever we decide to do next. I know we've still got a lot to learn, but hopefully each project/release will see an improvement. Onwards!