---
author: Alex
date: "2013-07-25T22:40:39Z"
description: ""
draft: false
slug: jamaica-2013
title: Jamaica 2013
summary: We took a trip to Jamaica, it was hot!
---

> First of all Google Drive doesn't let you create documents while offline, which is why I'm writing this up as an email instead of a Google doc.
> 
> I am totally not geared up for the kind of weather we've had here in Jamaica, although the natives have routinely commented that it's not very warm, I can assure you that to me, ~30°C from 7am to 7pm (even then only dropping to 25°C over night) feels very hot. Of course this is the temperature without the sun, which feels like it easily adds another 10°C. The only escape is to sit by the sea and enjoy the cooling breeze that keeps things manageable.
> 
> Sat on the beach where I can see the end of the Montego Bay airport runway is pretty cool, especially the roar of the jet engines carrying across the bay.
> 
> I burnt the first day I was here, totally underestimating the power of the Caribbean sunshine.
> 
> I have never known a sea so calm as the one lapping in front of me as I write this, at times it appears almost motionless, like a still lake. When it does get 'choppy', the waves are still only a few inches tall.
> 
> Only when explicitly pointed out to you, do you realise how nice it is to be staying at an adults only resort. Everyone is calm and grown up (whilst still letting their hair down and having a good time) and there are no screaming kids anywhere within earshot.
> 
> There are a lot of Americans here, mostly hugely polite but still often amusing with the things they come out with. One guy's total bemusement at what cricket was still makes me laugh to myself now.
> 
> There are so many kindles, everywhere you look someone has a kindle, far more so than actual, real books. The transition has happened, or at least has happened with the types of people who come to a resort like this. Also massively impressed with the readability of a Kindle in direct sunlight, far far superior to this Nexus 7 that I'm writing this on.

That is the blog post that I wrote from the beaches of Jamaica back in March. I was supposed to publish it not long after I got back, but never got around to it.

Anyway, here it is.