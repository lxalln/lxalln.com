---
author: Alex
categories:
- Coffee
- Tolerance
- Learning
- Twiglets
date: "2013-01-12T11:30:43Z"
description: ""
draft: false
slug: learning-to-tolerate-coffee
tags:
- Coffee
- Tolerance
- Learning
- Twiglets
title: Learning To Tolerate Coffee
summary: I don’t know whether I should blame my parents, or if it’s just ‘one of those things’, but I can’t stand coffee. I have tried multiples times in the past to drink the stuff, all to no avail. However, I am trying again, all because of Twiglets.
---

{{< figure src="/images/11368_coffee-beans.jpg" width="100%" >}}

I don’t know whether I should blame my parents, or if it’s just ‘one of those things’, but I can’t stand coffee. I have tried multiples times in the past to drink the stuff, all to no avail. However, I am trying again, all because of Twiglets.

I don’t like Twiglets. At least, that’s my official stance, and I would even go as far as to say I hate them. However, I used to really like their form and their salty dryness. So I used to nibble on one or two sometimes before the taste would get to me and I’d stop.

Mid-way through 2012 I moved in with my girlfriend, who has the opposite opinion to me on Twiglets. So there are pots of Twiglets in our flat. It was amusing to all those who witnessed me eating these torrid things, I would pull some crazy screwed up face of disgust, but continue eating them anyway.

Last week, whilst clearing up from a night in front of the TV, I stole a couple of Twiglets out of the pack, on their way to being put away in the cupboard. After they were safely away and the drawer shut, it dawned on me that I hadn’t screwed up my face, I hadn’t even thought about the fact I didn’t like them, I’d just quite happily eaten a couple and enjoyed it.

So, to coffee. I figure that if I do with coffee as I inadvertently did with Twiglets, then I could come to enjoy this drink that seemingly all grown-ups drink. Every morning I am having a cup of black, sweetened coffee. At the time of writing, I have done this for 5 consecutive mornings, finishing my cup each time.

I am determined to acquire the taste.

I will write another post in a few weeks to update on my progress. Wish me luck!