---
author: Alex
categories:
- Car
- Life
- Work
date: "2008-08-31T15:41:58Z"
description: ""
draft: false
slug: a-brief-catch-up
tags:
- Car
- Life
- Work
title: A brief catch up
---

Its been a long time since I updated this blog, almost a year in fact. A lot has happened since then:

Finished university.
Moved to Evesham.
Started work in Chipping Campden.
Bought a new(ish) car.

I managed to graduate from university with a 2:1 degree, which to be honest, I was a little disappointed with. My disseration was awful, getting a miserable 44%. The disseration was worth 30% of my degree. Which serves me right, I didn't work hard enough.

Moving to my flat went far smoother than things normally do for me, and I'm enjoying my time living on my own again.

Work is good now, took me a while to fit in again, but things are getting better as each month goes past.

New car! Finally. I had a Golf, but it was 15 years old, and it broke, so I have taken out a loan (so much stress, another post on that coming up soon), to buy myself a car that hopefully won't break.

Right, I'm going to try really hard to keep posting to here more often.

TTFN.