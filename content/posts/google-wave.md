---
author: Alex
categories:
- Google Wave
- Google
- Wave
date: "2009-05-30T18:09:00Z"
description: ""
draft: false
slug: google-wave
tags:
- Google Wave
- Google
- Wave
title: Google Wave
---

<img class="aligncenter size-full wp-image-77" src="http://www.google.com/intl/en_ALL/images/logo.gif" title="Google Logo" />

I just finished watching the <a href="http://wave.google.com/">Google Wave</a> video on <a href="http://www.youtube.com">YouTube</a>, <a href="http://www.youtube.com/watch?v=v_UyVmITiYQ">here</a>.

Google have come along with a new technology called Wave. Watch the video to find out more about it (be warned, its a video from the <a href="http://code.google.com/events/io/">Google I/O</a> developer conference, so its a little techy). Basically they have stepped back from the way we currently communicate with each other (e-mail, IM, SMS) etc and, reinvented it. Its hard to explain, just watch the <a href="http://www.youtube.com/watch?v=v_UyVmITiYQ">video</a> (its long though).

There are times when I think <a href="http://www.google.com">Google</a> are becoming the new <a href="http://www.microsoft.com">Microsoft</a>, the way they're taking over everything and the way they buy new companies, however... Google keep proving me wrong, time and time again they surprise me.

I'll try and summarise the features that I can remember (guess they're the good ones):
<ul>
	<li><strong>Live</strong> - <em>All communication when using Wave is live, even the typing, so no more "Buddy is typing...". Waves come and go in your inbox, even the search is live.</em></li>
	<li><strong>Open Source</strong> - <em>Woo! This means that other people can create their own Wave servers. Different wave servers can communicate with each other, using something called <strong>Foundation</strong></em></li>
	<li><strong>Collaboration</strong> - <em>A wave is basically a smart document, or conversation. A smart docusation if you would. All the people involved with the Wave can edit the Wave at the same time and all the changes are displayed live on all the clients. Impressive :)</em></li>
	<li><strong>Playback</strong> - <em>When you are added to a Wave after it has already started, you will see all the edits/comments/conversations in the Wave already. Google have come up with a playback feature, which lets you see how the Wave was constructed. Another impressive feature.</em></li>
	<li><strong>Extensions</strong> - <em>Google demonstrated some pretty cool extensions, check them out in the video.</em></li>
	<li><strong>Rosy</strong> - <em>Live Translation! This was probably the most impressive feature of Wave, Google demonstrated two people conversing, one in French and another in English, Wave was translating what was written, <strong>in real time!</strong></em></li>
</ul>
<span style="color:#7f817e;"> </span>

Overall I was very impressed with Google Wave, obviously as a developer I find this thing interesting anyway, but hopefully this will change the way people communicate world wide. For the better. Open protocols, Open source.

Google have impressed me today.